const fs = require('fs');

const mandatoryConfigs = [
  'grant_type',
  'client_id',
  'client_secret',
];
/**
 * Check that mandatory config params are set
 * @param {Config} config
 */
const validConfig = (config) => (
  mandatoryConfigs.every((c) => !!config[c])
);

/**
 * Method to check if given file path exist
 * @param {String} path
 */
const fileExists = (path) => (
  new Promise((resolve) => {
    fs.access(path, fs.F_OK, (err) => {
      if (err) {
        console.error(err);
      }
      resolve(!err);
    });
  })
);

module.exports = {
  fileExists,
  validConfig,
  mandatoryConfigs,
};
