const express = require('express');
const path = require('path');
const { EOL } = require('os');
const exphbs = require('express-handlebars');
const config = require('./config');
const utils = require('./utils');

const app = express();

/**
 * Set handlebars config
 */
app.engine('handlebars', exphbs({
  defaultLayout: 'main',
  layoutsDir: path.join(`${path.join(__dirname, 'views')}`),
  helpers: {
    json: (context) => JSON.stringify(context),
  },
}));
app.set('view engine', 'handlebars');
app.set('views', path.join(__dirname, 'views'));

/** Public path for assets */
app.use(express.static('public'));

/** init api cache from cached data if exists */
require('./api/cache').initCache(path.join(__dirname, '/'));

/** Start API polling */
require('./api/request').run();

/** Add controller routes */
const SummaryController = require('./features/summary/controller');
const HealthzContoller = require('./features/healthz/controller');
const ReportController = require('./features/report/controller');

app.use(SummaryController(express.Router));
app.use(HealthzContoller(express.Router));
app.use(ReportController(express.Router));

/** Start server */
if (utils.validConfig(config)) {
  app.listen(config.PORT, () => {
    console.log(`Example app listening at http://localhost:${config.PORT}`);
  });
} else {
  console.error(`Following configs are required for this service to work:${EOL}${utils.mandatoryConfigs.join(EOL)}`);
}
