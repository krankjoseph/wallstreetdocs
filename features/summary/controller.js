const { getStatusData } = require('../../api/cache');

const render = (req, res) => {
  res.render('summary', {
    cacheData: getStatusData(),
  });
};

/**
 * Endpoint for summary page
 * @param {Router} express
 */
module.exports = (express) => {
  const router = express();
  router.get('/', render);
  return router;
};
