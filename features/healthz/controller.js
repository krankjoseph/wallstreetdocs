const healthz = (req, res) => {
  res.send('ok');
};

/**
 * Simple end point to check that server is alive
 * @param Router express
 */
module.exports = (express) => {
  const router = express();
  router.get('/healthz', healthz);
  return router;
};
