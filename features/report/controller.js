const { getStatusData } = require('../../api/cache');

const render = (req, res) => {
  res.render('report', {
    cacheData: getStatusData(),
  });
};

/**
 * Endpoint for report page
 * @param {Router} express
 */
module.exports = (express) => {
  const router = express();
  router.get('/report', render);
  return router;
};
