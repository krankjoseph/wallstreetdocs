const dotenv = require('dotenv');

dotenv.config();

/**
 * Config settings for running the service
 * {PORT} the port the service will listen. Defaults to 3000 if not given in env variables
 * {ROOT} service root directory
 * {oAuthServer} endpoint for oAuth request
 * {apiServer} endpoint for api requests
 * {grant_type, client_id, client_secret} required params for oAuth request
 * {apiPollInterval} interval for fetching report data in seconds
 */
module.exports = {
  PORT: process.env.PORT || 3000,
  ROOT: process.env.PWD,
  oAuthServer: 'https://staging-authentication.wallstreetdocs.com',
  apiServer: 'https://staging-gateway.priipcloud.com/api/v2.0/gateway',
  grant_type: process.env.grant_type,
  client_id: process.env.client_id,
  client_secret: process.env.client_secret,
  apiPollInterval: 30,
};
