/* eslint-disable prefer-template */
/* eslint-disable no-var */
/* eslint-disable object-shorthand */
/* eslint-disable func-names */
/* eslint-disable no-undef */
/* eslint-disable prefer-arrow-callback */

/** Build table that contains health info of ok services */
var supportedChecks = [
  'Server Free Disk Space',
  'Server Free System Memory',
  'Node Event Loop Latency',
  'PM2 Process Status',
];
function buildStatusTable(chartData) {
  var data = null;
  var header = '<tr><th>host/node</th><th>Disk</th><th>Memory</th><th>Latency</th><th>Pm2</th></tr>';

  console.log('buildStatusTable');
  data = chartData.service_reports.reduce(function (response, report) {
    var tableData = response;
    var hostName = null;
    if (report.status_code === 200 && report.status_text === 'OK') {
      hostName = report.host.name;
      console.log(report);
      tableData += '<tr class="host"><td colspan=5>' + hostName + '</td></tr>';
      report.nodes.forEach((node) => {
        tableData += '<tr class="node"><td>' + node.web_node + '</td>';
        node.checks.filter((check) => supportedChecks.includes(check.name)).forEach((check) => {
          tableData += '<td><div class="state ' + check.state + '" title="' + check.name + ' (' + check.state + ')"></div></td>';
        });
        tableData += '</tr>';
      });
    }
    return tableData;
  }, '');
  $('#statusTable').html('<table>' + header + data + '</table>');
}

/** Builds detaled table for selected service status */
function showDetailedTable(category, statusCode) {
  var data = null;

  console.log('showDetailedTable', category, statusCode);
  data = chartData.service_reports.reduce(function (response, report) {
    var tableData = response;
    var hostName = null;
    if (report.status_code === statusCode && report.status_text.includes(category)) {
      hostName = report.host.name;
      tableData += '<tr class="host"><td colspan=2>' + hostName + '</td></tr>';
      report.nodes.forEach((node) => {
        tableData += '<tr class="node"><td>' + node.web_node + '</td><td>' + node.status_text + '</td></tr>';
      });
    }
    return tableData;
  }, '<table>');
  $('#detailTable').html(`${data}</table>`);
}

// Create the chart
function buildChart(chartData) {
  /** Reduce that report data to per status code/text objects */
  var data = chartData.service_reports.reduce((response, report) => {
    var statusText = report.status_text;
    if (statusText.includes('invalid json')) {
      statusText = 'invalid json';
    } else if (statusText.includes('nodes reported an error')) {
      statusText = 'nodes reported an error';
    }
    if (!Object.hasOwnProperty.call(response,statusText)) {
      response[statusText] = {
        statusCode: report.status_code,
        y: 0,
      };
    }

    response[statusText].y += 1;

    return response;
  }, {});

  /** Generage series from reduced data */
  var series = Object.keys(data).reduce((s, key) => {
    s.push({
      name: key,
      statusCode: data[key].statusCode,
      y: data[key].y,
    });
    return s;
  }, []);

  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Status Data',
    },
    subtitle: {
      text: 'Click the columns to view detailed data table',
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: 'category',
      categories: series.map((a) => a.name),
    },
    yAxis: {
      title: {
        text: 'Count',
      },

    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}',
        },
        point: {
          events: {
            click: function () {
              showDetailedTable(this.category, this.options.statusCode);
            },
          },
        },
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>',
    },

    series: [
      {
        name: 'Statuses',
        colorByPoint: true,
        data: series,
      },
    ],
  });
}
