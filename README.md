# Wallstreetdocs

### How to run

* cd to the folder and run `npm install` and then `npm start`
* create `.env` file that includes `PORT`, `grant_type`, `client_id`, `client_secret` in format
`PORT=****`
`grant_type=****`
`client_id=****`
`client_secret=****  `
* by default the service is available at `localhost:3000`
* if any of `grant_type`, `client_id`, `client_secret` is missing fron `.env` the service wont start

## Configs

Service has `config.js` file that includes values to setup the service
* {PORT} the port the service will listen. Defaults to 3000 if not given in env variables
* {ROOT} service root directory
* {oAuthServer} endpoint for oAuth request
* {apiServer} endpoint for api requests
* {grant_type, client_id, client_secret} required params for oAuth request (taken from env variables)
* {apiPollInterval} interval for fetching report data in seconds. Defaults to 30s

### Used packages and versions
Requirements
* express 4
* node 12

Others
* Axios for http requests (more familiar with request, but it's deprecated)
* eslint with airbnb-base for linting
* dotenv for env configs
* express-handlebars for templates

### Technology

The service is writen in ES6/7 NodeJS v12 with Express v4 for API endpoints.
The report data is fetched on internvals (default 30s) and cached both in memory and file so it can be used for multiple
requests and so that there would be data available after restart.

Front end is ES5 with bootstrap and jquery. Some of the defaults are templated on server side using handlebars.
Chart is created using `Highcharts` as I've used it in my current and previous roles.

### Improvements over time

The service includes console logs/errors and would be better to use something like `winston` to handle the logging.
That would allow the use of multiple log levels driven by config/env variables and direct streaming to external services.

I didn't write any tests for this so they would be quite important to add.