const fs = require('fs');
const { fileExists } = require('../utils');

let statusData = null;
let filePath = '';

/**
 * Return latest cached data
 */
const getStatusData = () => statusData;

/**
 * Update cache data and store it to file
 * @param {JSON} data
 */
const setStatusData = (data) => {
  statusData = data;
  fs.writeFile(filePath, JSON.stringify(data), 'utf8', (err, result) => {
    if (err) {
      console.error(err);
    } else {
      console.log(result);
    }
  });
};

/**
 * Init cache from existing file if one exists
 * @param {String} rootPath
 */
const initCache = async (rootPath) => {
  filePath = `${rootPath}statusData.json`;
  if (await fileExists(filePath)) {
    fs.readFile(filePath, 'utf8', (err, result) => {
      let data = null;
      if (err) {
        console.error(err);
      } else {
        try {
          const r = JSON.parse(result);
          data = r;
        } catch (e) {
          console.error(e);
        }
      }
      statusData = data;
    });
  } else {
    console.info(`Cache file ${filePath} doesn't exist`);
  }
};

module.exports = {
  initCache,
  getStatusData,
  setStatusData,
};
