const axios = require('axios');
const cache = require('./cache');
const config = require('../config');

let bearer = null;
let tokenExpires = null;
let jobId = null;

/**
 * Check if token has expired
 * Use 2s buffer so we wouldn't send request with almost expired token
 * as it might fail due to network delays
 */
const isTokenExpired = () => (
  new Date().getTime() >= (tokenExpires - 2000)
);

/** Gets auth token used for other requests */
const getToken = () => (
  new Promise((resolve, reject) => {
    axios.post(`${config.oAuthServer}/oauth/token`, {
      grant_type: config.grant_type,
      client_id: config.client_id,
      client_secret: config.client_secret,
    })
      .then((response) => {
        bearer = response.data.access_token;
        tokenExpires = new Date().getTime() + (response.data.expires_in * 1000);
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  })
);

/** Background tasks that fetches data on interval and token as needed */
const run = async () => {
  try {
    if (!bearer || isTokenExpired()) {
      console.log('getToken');
      await getToken();
    }
    getJobId();
  } catch (e) {
    console.error(e);
  }
};

/** Gets report for specific jobId */
const getServiceReport = () => {
  console.log('getServiceReport', jobId);
  axios.get(`${config.apiServer}/reports/status/service/${jobId}`, {
    headers: {
      Authorization: `Bearer ${bearer}`,
    },
  })
    .then((response) => {
      cache.setStatusData(response.data);
      jobId = null;
      setTimeout(run, config.apiPollInterval * 1000);
    })
    .catch((error) => {
      console.error(error);
      setTimeout(run, config.apiPollInterval * 1000);
    });
};

/** Gets jobId for status report */
const getJobId = () => {
  console.log('getJobId');
  axios.post(`${config.apiServer}/reports/status/service`, {}, {
    headers: {
      Authorization: `Bearer ${bearer}`,
    },
  })
    .then((response) => {
      console.log(response.data);
      jobId = response.data.job_id;
      setTimeout(getServiceReport, 5000);
    })
    .catch((error) => {
      console.error(error);
      bearer = null;
      setTimeout(run, config.apiPollInterval * 1000);
    });
};

module.exports = {
  run,
};
